#/!bin/bash
REMOTE_PROJECT_ID=31893344
pipeline_id=$(curl -k --location --header "PRIVATE-TOKEN: glpat-pd-Acr3vgPgV3dv-XhEY" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/pipelines/" | jq -r '.[] | "\(.id)"' | head -1)
job_for_play=$(curl -k --location --header "PRIVATE-TOKEN:  glpat-pd-Acr3vgPgV3dv-XhEY" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/pipelines/$pipeline_id/jobs" | jq -r '.[] | "\(.id)"')


curl --request PUT --header "PRIVATE-TOKEN: ${MY_TOKEN}" \
     "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/variables/imageTAG" --form "key=imageTAG" --form "value=${imageTAG}"

curl --request PUT --header "PRIVATE-TOKEN: ${MY_TOKEN}" \
     "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/variables/nginxTAG" --form "key=nginxTAG" --form "value=${nginxTAG}"

curl --request PUT --header "PRIVATE-TOKEN: ${MY_TOKEN}" \
     "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/variables/CI_COMMIT_SHORT_SHA" --form "key=CI_COMMIT_SHORT_SHA" --form "value=${CI_COMMIT_SHORT_SHA}"

curl --request PUT --header "PRIVATE-TOKEN: ${MY_TOKEN}" \
     "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/variables/CI_REGISTRY_IMAGE" --form "key=CI_REGISTRY_IMAGE" --form "value=${CI_REGISTRY_IMAGE}"

curl --request PUT --header "PRIVATE-TOKEN: ${MY_TOKEN}" \
     "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/variables/nginx" --form "key=nginx" --form "value=${nginx}"

curl -k --request POST --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/jobs/${job_for_play}/play"
