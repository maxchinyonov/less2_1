<h2 class="code-line" data-line-start=0 data-line-end=1 ><a id="__1_0"></a>Описание задания №1</h2>
<h4 class="code-line" data-line-start=2 data-line-end=3 ><a id="__21__4___2__2_____2__________2"></a>Есть прокт 2.1 там 4 джобы и 2 стейджа. 2 джобы билдят образы а 2 стартуют композ. Так же в этом проекте есть Докер-композ.</h4>
<p class="has-line-data" data-line-start="3" data-line-end="4">Что делаем</p>
<ol>
<li class="has-line-data" data-line-start="4" data-line-end="5">Создаем чистый проект и копируем наш композ</li>
<li class="has-line-data" data-line-start="5" data-line-end="8">Пишем там короткий ямл в котором 1 стейдж 1 джоба<br>
Джобы должна быть с ручным запуском (вен мануал)<br>
Джобы должна в скриптах делать - докер композ ап минус ди</li>
<li class="has-line-data" data-line-start="8" data-line-end="14">В первом проекте берем 2 джобы стейджа РАН и удаляем скрипты так как теперь в них должен быть запуск по апи нового проекта с передачей переменных ( так как в композе есть теги имеджа ввиде sha) и запуск мануальной джобы так же по АПИ.<br>
Так же в джобах билда передаем вывод докер билд в файл и сохраняем его как артифакт<br>
4 Так как мы во второй проект не перенесли конфиг нжингса значит в джобах для сборки мы собираем и его ибо конфиг нужен нам в имедже<br>
5 Так как мы собираем нжингс значит в композе в новом проекте не должно быть билда нжингса а просто указан имедж.<br>
Задача со*</li>
</ol>
<p class="has-line-data" data-line-start="14" data-line-end="15">Написать скрипт который при запуске (с хоста) выведит последнию зафейленую джобу с именем build_prod (если у вас имя джобы другое подставьте его)</p>
<p class="has-line-data" data-line-start="17" data-line-end="21">Полезные ссылки<br>
<a href="https://docs.gitlab.com/ee/api/pipelines.html">https://docs.gitlab.com/ee/api/pipelines.html</a><br>
<a href="https://habr.com/ru/post/347808/">https://habr.com/ru/post/347808/</a><br>
<a href="https://stedolan.github.io/jq/tutorial/">https://stedolan.github.io/jq/tutorial/</a></p>
<h3 class="code-line" data-line-start=24 data-line-end=25 ><a id="_24"></a>Решение</h3>
<p class="has-line-data" data-line-start="26" data-line-end="29"><a href="https://gitlab.com/maxchinyonov/less2_1">https://gitlab.com/maxchinyonov/less2_1</a><br>
Тут собираем nginx + django<br>
Соответственно для каждой сборки для каждой ветки своя джоба в .gitlab.ci</p>
<p class="has-line-data" data-line-start="30" data-line-end="31">В ранах просто скрипт check_var.sh</p>
<p class="has-line-data" data-line-start="32" data-line-end="33">Описание файла “check_var.sh” :</p>
<p class="has-line-data" data-line-start="34" data-line-end="35">$pipeline_id и $job_for_play вытащены по api через курлы с Less2_4.</p>
<p class="has-line-data" data-line-start="36" data-line-end="37">все строки с “echo” добавил для наглядности проверки, чтобы потом можно было посмотреть каждую джобу в пайпе, где нам будет видно, какие значения принимают эти переменные в зависимости от ветки и увидеть что мы потом запостили через gitlab-api. (тип того $nginx=8000 или 8001, $imageTAG=dev или prod, $nginxTAG=dev или prod)</p>
<p class="has-line-data" data-line-start="38" data-line-end="41">Потом тут же курлами делаем POST и создаём наши переменные в проекте Less2_4<br>
(У меня PUT, т.к это не первый run и переменные уже существуют в Less2_4. PUT’ом просто переопределяем, если значения не сходятся с существующими. Можно делать POST, потом удалять, но не принципиально)<br>
Последним курлом запускаем джобу в Less2.4</p>
<p class="has-line-data" data-line-start="43" data-line-end="46"><a href="https://gitlab.com/maxchinyonov/less2_4">https://gitlab.com/maxchinyonov/less2_4</a><br>
тут 1 джоба, которая просто запускает композ<br>
Композ тянет образы nginx, django при помощи переданных переменных из регистри Less 2.1</p>
<p class="has-line-data" data-line-start="48" data-line-end="56">Для проверки:<br>
Пайплайны. Последние актуальные.<br>
<a href="https://gitlab.com/maxchinyonov/less2_1/-/pipelines">https://gitlab.com/maxchinyonov/less2_1/-/pipelines</a><br>
427247242 - dev<br>
427239176 - prod<br>
<a href="https://gitlab.com/maxchinyonov/less2_4/-/pipelines">https://gitlab.com/maxchinyonov/less2_4/-/pipelines</a><br>
427225690 - запуск с имеджами с дева<br>
427153700 - запуск с имеджами с прода</p>
<hr>
<p class="has-line-data" data-line-start="57" data-line-end="59">Задача со *<br>
Написал скрипт, который ищет и выводит последнюю джобу для указанного проекта по параметрам, которые мы ей задаём:</p>
<p class="has-line-data" data-line-start="60" data-line-end="62">Имя джобы - любое имя джобы, которая есть в нашем проекте<br>
Статус джобы - success/failed</p>
<p class="has-line-data" data-line-start="63" data-line-end="64"><a href="https://gitlab.com/maxchinyonov/less2_4/-/blob/main/show_last_job_dynamic.sh">https://gitlab.com/maxchinyonov/less2_4/-/blob/main/show_last_job_dynamic.sh</a></p>
<h2 class="code-line" data-line-start=68 data-line-end=69 ><a id="__2_68"></a>Описание задания №2</h2>
<p class="has-line-data" data-line-start="69" data-line-end="74">В прошлом задании мы создавали файл и сохраняли его как Артифакт. Но так у гитлаба нет возможности передавать артифакты между проектами, мы это сделаем с помощью АПИ.<br>
С прошлого задания у нас должен остаться проект где была всего одна джоба с раном проекта.<br>
Необходимо что бы 1 скрипт в этой джобе скачивал последний артефакт из нашего основного проекта.<br>
Напомню<br>
У нас есть 2 проекта. Первый запускает второй</p>
<h3 class="code-line" data-line-start=75 data-line-end=76 ><a id="_75"></a>Решение</h3>
<p class="has-line-data" data-line-start="77" data-line-end="79"><a href="https://gitlab.com/maxchinyonov/less2_1">https://gitlab.com/maxchinyonov/less2_1</a><br>
Тут генерим артефакты (к примеру сделал для каждой джобы стейджа build по артефакту, т.е 4 штуки) 2 на мастер и 2 на дев.</p>
<p class="has-line-data" data-line-start="81" data-line-end="83"><a href="https://gitlab.com/maxchinyonov/less2_4">https://gitlab.com/maxchinyonov/less2_4</a><br>
Тут их все вытягиваем по api и сохраняем на локальную машину.</p>
<p class="has-line-data" data-line-start="84" data-line-end="87">Используем для этого скрипт<br>
<a href="https://gitlab.com/maxchinyonov/less2_4/-/blob/main/download_artifact.sh">https://gitlab.com/maxchinyonov/less2_4/-/blob/main/download_artifact.sh</a><br>
В этом скрипте</p>
<ol>
<li class="has-line-data" data-line-start="87" data-line-end="88">определяем последний пайп в проекте less2_1</li>
<li class="has-line-data" data-line-start="88" data-line-end="89">Последний id джобы для каждой джобы.</li>
<li class="has-line-data" data-line-start="89" data-line-end="92">Ну и потом соответственно , если у less2_1 был пуш в мастер, то тянем джобы с мастера которые с пометкой “prod”, если пуш в дев, то с дева с пометкой “dev”.<br>
Чтобы определить в какую ветку был пуш в Less2_1 тянем новую переменную (в Less2_4 её назвал $REMOTE_PROJECT_REF).<br>
По сути это переименованная предопределённая переменная $CI_COMMIT_REF_NAME из Less2_1</li>
</ol>
<p class="has-line-data" data-line-start="94" data-line-end="95">По умолчанию артефакты сохраняются в директории в которой запускается гитлаб раннер, если в пути курла не указывать иное. Но это не совсем удобно, т.к надо искать где она находится. Я создал папку artifacts в нашем проекте и дал раннеру права на запись в неё. Соответственно курлами указываем путь до этой папки и кидаем туда наши артефакты. Конечно же для удобства для пути используем переменную.</p>
<p class="has-line-data" data-line-start="96" data-line-end="97">В итоге в папке artifacts после двух запусков(из дева и мастера) получаем следующее (см. artifacts.jpeg)</p>
<p class="has-line-data" data-line-start="99" data-line-end="104">Последние пайпы для проверки<br>
Запускаем 2.1 с мастера<br>
<a href="https://gitlab.com/maxchinyonov/less2_1/-/pipelines/432502331">https://gitlab.com/maxchinyonov/less2_1/-/pipelines/432502331</a><br>
Он запускает пайплайн в 2.4<br>
<a href="https://gitlab.com/maxchinyonov/less2_4/-/pipelines/432503911">https://gitlab.com/maxchinyonov/less2_4/-/pipelines/432503911</a></p>
<p class="has-line-data" data-line-start="106" data-line-end="110">Запускаем 2.1 с дева<br>
<a href="https://gitlab.com/maxchinyonov/less2_1/-/pipelines/432504916">https://gitlab.com/maxchinyonov/less2_1/-/pipelines/432504916</a><br>
Он запускает пайплайн в 2.4<br>
​https://gitlab.com/maxchinyonov/less2_4/-/pipelines/432506420</p>
