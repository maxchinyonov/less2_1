#/!bin/bash
REMOTE_PROJECT_ID=31893344


curl --request POST --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/pipeline?ref=main" \
--form "variables[][key]=imageTAG" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${imageTAG}" \
--form "variables[][key]=nginxTAG" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${nginxTAG}" \
--form "variables[][key]=CI_COMMIT_SHORT_SHA" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${CI_COMMIT_SHORT_SHA}" \
--form "variables[][key]=CI_REGISTRY_IMAGE" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${CI_REGISTRY_IMAGE}" \
--form "variables[][key]=nginx" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${nginx}" \
--form "variables[][key]=tag_nginx_prod" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${tag_nginx_prod}" \
--form "variables[][key]=tag_prod" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${tag_prod}" \
--form "variables[][key]=tag_nginx_dev" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${tag_nginx_dev}" \
--form "variables[][key]=tag_dev" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${tag_dev}" \
--form "variables[][key]=REMOTE_PROJECT_REF" \
--form "variables[][variable_type]=env_var" \
--form "variables[][value]=${CI_COMMIT_REF_NAME}"


pipeline_id=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/pipelines/" | jq -r '.[] | "\(.id)"' | head -1)
job_for_play=$(curl -k --location --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/pipelines/$pipeline_id/jobs" | jq -r '.[] | "\(.id)"')

curl -k --request POST --header "PRIVATE-TOKEN: ${MY_TOKEN}" "https://gitlab.com/api/v4/projects/${REMOTE_PROJECT_ID}/jobs/${job_for_play}/play"
